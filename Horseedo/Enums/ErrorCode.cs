﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Horseedo.Enums
{
    public enum ErrorCode
    {
        UNKNOWN = 0,
        BANNED = 1,
        UNSUPPORTED_ROLE = 2,
        UNKNOWN_ROLE = 3,
        LOGIN_FAILED = 4,
        USER_EXISTS = 5,
        USER_NOT_ACTIVATED = 6,
        WRONG_ACTIVATE_CODE = 7,
        UNKNOWN_EMAIL = 8,
        UNKNOWN_USER = 9,
        WRONG_PASSWORD = 10,
    }

}
