﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Horseedo.Enums
{
    public enum UserRole
    {
        ADMIN = 1,
        USER = 2
    }
}
