﻿using Horseedo.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;

namespace Horseedo.Exceptions
{
    [Serializable]
    abstract public class HorseedoException : Exception
    {
        public HttpStatusCode StatusCode { get; set; }
        public ErrorCode ErrorCode { get; set; }

        public HorseedoException(HttpStatusCode statusCode, ErrorCode errorCode, String message) : base(message)
        {
            this.StatusCode = statusCode;
            this.ErrorCode = errorCode;
        }
    }

    [Serializable]
    public class UnauthorizedHorseedoException : HorseedoException
    {
        public UnauthorizedHorseedoException(ErrorCode errorCode, String message)
            : base(HttpStatusCode.Unauthorized, errorCode, message)
        {
            this.ErrorCode = errorCode;
        }
    }

    [Serializable]
    public class BadRequestHorseedoException : HorseedoException
    {
        public BadRequestHorseedoException(ErrorCode errorCode, String message)
            : base(HttpStatusCode.BadRequest, errorCode, message)
        {
            this.ErrorCode = errorCode;
        }
    }

    [Serializable]
    public class NotFoundHorseedoException : HorseedoException
    {
        public NotFoundHorseedoException(ErrorCode errorCode, String message)
            : base(HttpStatusCode.NotFound, errorCode, message)
        {
            this.ErrorCode = errorCode;
        }
    }

    [Serializable]
    public class InternalErrorHorsedoException : HorseedoException
    {
        public InternalErrorHorsedoException(ErrorCode errorCode, String message)
            : base(HttpStatusCode.InternalServerError, errorCode, message)
        {
            this.ErrorCode = errorCode;
        }
    }

}
