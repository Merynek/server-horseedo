﻿using Horseedo.Cache;
using Horseedo.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Horseedo.Services
{
    public class OnStartedService
    {
        private readonly HorseedoDBContext _context;
        private readonly UserCache _userCache;

        public OnStartedService(HorseedoDBContext context, UserCache userCache)
        {
            _context = context;
            _userCache = userCache;
            SetupUserCache();
        }

        private void SetupUserCache()
        {
            var users = _context.User.Where(u => u.Banned).ToList();
            if (users != null)
            {
                _userCache.CreateUserCache(users);
            }
        }

    }
}
