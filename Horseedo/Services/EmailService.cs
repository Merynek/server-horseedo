﻿using Horseedo.Enums;
using Horseedo.Models.AppSettings;
using Horseedo.Models.Database;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Text;
using System.Threading.Tasks;

namespace Horseedo.Services
{
    public class EmailService
    {
        private readonly EmailSettings _emailSettings;
        private readonly CompanyInfo _companyInfo;
        private readonly ClientSettings _clientSettings;

        public EmailService(IOptions<EmailSettings> emailSettings, IOptions<ClientSettings> clientSettings, IOptions<CompanyInfo> companyInfo)
        {
            _emailSettings = emailSettings.Value;
            _clientSettings = clientSettings.Value;
            _companyInfo = companyInfo.Value;
        }

        public async Task SendForgetPassword(string email, string password)
        {
            var builder = GetTemplateByEmailType(EmailType.FORGET_PASSWOD);

            builder.Replace("{password}", password);

            await SendEmailAsync(email, "Obnovení hesla", builder.ToString());
        }

        public async Task SendRegistration(User user, string activated_code)
        {
            var builder = GetTemplateByEmailType(EmailType.REGISTRATION);
            string activateLink = _clientSettings.Url + "/activate/" + activated_code;

            builder.Replace("{username}", user.Username);
            builder.Replace("{name}", user.Name);
            builder.Replace("{surname}", user.Surname);
            builder.Replace("{activateLink}", activateLink);

            await SendEmailAsync(user.Email, "Registrace", builder.ToString());
        }

        private async Task SendEmailAsync(string email, string subject, string content, Attachment attachment = null)
        {
            try
            {
                var from = new MailAddress(_emailSettings.Sender, "Horse - info");
                var to = new MailAddress(email);
                var msg = new MailMessage(from, to);

                msg.Attachments.Add(GetLogoAttachment());
                if (attachment != null)
                {
                    msg.Attachments.Add(attachment);
                }
                msg.IsBodyHtml = true;
                msg.Subject = subject;
                msg.Body = CreateLayout(subject, content);

                using (var client = new SmtpClient(_emailSettings.Host, _emailSettings.Port))
                {
                    client.Credentials = new NetworkCredential(_emailSettings.Sender, _emailSettings.Password);
                    client.EnableSsl = _emailSettings.EnableSsl;

                    await client.SendMailAsync(msg);
                }

            }
            catch (Exception ex)
            {
                // TODO: handle exception
                throw new InvalidOperationException(ex.Message);
            }
        }

        private string CreateLayout(string subject, string content)
        {
            var builder = new StringBuilder();

            using (var reader = File.OpenText("Utils/Email/Templates/layout.html"))
            {
                builder.Append(reader.ReadToEnd());
            }

            builder.Replace("{email_subject}", subject);
            builder.Replace("{email_content}", content);
            builder.Replace("{company_name}", _companyInfo.Name);
            builder.Replace("{company_mobile}", _companyInfo.Mobile);
            builder.Replace("{company_mobile_nice}", _companyInfo.MobileNice);
            builder.Replace("{company_email}", _companyInfo.Email);

            return builder.ToString();
        }

        private Attachment GetLogoAttachment()
        {
            Attachment attachment = new Attachment("Utils/Email/Templates/company_logo.png");
            attachment.ContentId = "companyEmailLogoContentId";

            return attachment;
        }

        private StringBuilder GetTemplateByEmailType(EmailType type)
        {
            var builder = new StringBuilder();
            string path = "Utils/Email/Templates/";

            switch (type)
            {
                case EmailType.REGISTRATION:
                    path += "registration.html";
                    break;
                case EmailType.FORGET_PASSWOD:
                    path += "forget_password.html";
                    break;
                default: return null;
            }

            using (var reader = File.OpenText(path))
            {
                builder.Append(reader.ReadToEnd());
            }

            return builder;
        }
    }

}
