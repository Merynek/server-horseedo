﻿using Horseedo.Models;
using Horseedo.Models.Database;
using Horseedo.Models.Request.Horse;
using System.Threading.Tasks;

namespace Horseedo.Services
{
    public class HorseService
    {
        private readonly HorseedoDBContext _context;

        public HorseService(HorseedoDBContext context)
        {
            _context = context;
        }

        public async Task<Horse> AddHorse(AddHorseRequest req)
        {
            var horse = new Horse();
            horse.Name = req.Name;
            _context.HorseAnimal.Add(horse);

            await _context.SaveChangesAsync();
            return horse;
        }
    }
}
