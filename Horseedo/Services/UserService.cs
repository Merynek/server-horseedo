﻿using Horseedo.Cache;
using Horseedo.Enums;
using Horseedo.Exceptions;
using Horseedo.Models;
using Horseedo.Models.AppSettings;
using Horseedo.Models.Database;
using Horseedo.Models.Request;
using Horseedo.Models.Request.Users;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Tokens;
using System;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace Horseedo.Services
{
    public class UserService
    {
        private readonly HorseedoDBContext _context;
        private readonly JWTSettings _jwtsettings;
        private readonly UserCache _userCache;
        private readonly EmailService _emailService;

        public UserService(HorseedoDBContext context, EmailService emailService, IOptions<JWTSettings> jwtsettings, UserCache userCache)
        {
            _context = context;
            _jwtsettings = jwtsettings.Value;
            _userCache = userCache;
            _emailService = emailService;
        }

        public Task<User> GetUser(string username, string password)
        {
            return _context.User
                 .Include(u => u.RefreshTokens)
                 .Where(user => user.Username == username && user.Password == Hash(password)).FirstOrDefaultAsync();
        }

        public Task<User> GetUser(int id)
        {
            return _context.User
                 .Where(user => user.Id == id).FirstOrDefaultAsync();
        }

        public async Task<bool> IsUserExist(string username, string email)
        {
            var user = await _context.User
                 .Where(user => user.Username == username || user.Email == email).FirstOrDefaultAsync();

            return user != null;
        }

        public async Task ActivateUser(ActiveUserRequest req)
        {
            var user = await _context.NotActivatedUser
                .Include(u => u.User)
                .Where(user => user.Code == req.ActivateCode).FirstOrDefaultAsync();

            if (user != null)
            {
                user.User.Activated = true;
                _context.Entry(user.User).State = EntityState.Modified;
                _context.NotActivatedUser.Remove(user);
                await _context.SaveChangesAsync();
                return;
            }
            throw new BadRequestHorseedoException(ErrorCode.WRONG_ACTIVATE_CODE, "Wrong activate code");
        }

        public async Task GenerateNewPassword(ForgetPasswordRequest req)
        {
            var user = await _context.User
                 .Where(user => user.Email == req.Email).FirstOrDefaultAsync();

            if (user != null)
            {
                string password = GenerateRandomPassword();
                user.Password = Hash(password);
                _context.Entry(user).State = EntityState.Modified;
                await _context.SaveChangesAsync();
                await _emailService.SendForgetPassword(req.Email, password);
                return;
            }
            throw new BadRequestHorseedoException(ErrorCode.UNKNOWN_EMAIL, "Unknown email");
        }

        public async Task ChangePassword(ChangePasswordRequest req, int userId)
        {
            var user = await GetUser(userId);

            if (user != null)
            {
                if (Hash(req.CurrentPass) == user.Password)
                {
                    user.Password = req.NewPassword;
                    _context.Entry(user).State = EntityState.Modified;
                    await _context.SaveChangesAsync();
                    return;
                }
                throw new BadRequestHorseedoException(ErrorCode.WRONG_PASSWORD, "Wrong password");
            }
            throw new BadRequestHorseedoException(ErrorCode.UNKNOWN_USER, "Unknown user");
        }

        public async Task ChangeInfo(ChangeUserInfoRequest req, int userId)
        {
            var user = await GetUser(userId);

            if (user != null)
            {
                user.Name = req.Name;
                user.Surname = req.Surname;
                user.PhoneNumber = req.PhoneNumber;
                _context.Entry(user).State = EntityState.Modified;
                await _context.SaveChangesAsync();
                return;
            }
            throw new BadRequestHorseedoException(ErrorCode.UNKNOWN_USER, "Unknown user");
        }

        public async Task RegistrationUser(RegistrationUserRequest req)
        {
            string activated_code = Hash(req.Username);
            var user = new User(req);
            user.Password = Hash(req.Password);
            _context.User.Add(user);
            await _emailService.SendRegistration(user, activated_code);

            await _context.SaveChangesAsync();

            var notActivatedUser = new NotActivatedUser();
            notActivatedUser.Code = activated_code;
            notActivatedUser.User = user;
            _context.NotActivatedUser.Add(notActivatedUser);

            await _context.SaveChangesAsync();
        }

        public async Task BanUser(int id_user, bool ban)
        {
            var user = await GetUser(id_user);
            user.Banned = ban;

            _userCache.SetBan(id_user, ban);
            _context.Entry(user).State = EntityState.Modified;

            await _context.SaveChangesAsync();
        }

        public async Task<RefreshToken> GenerateRefreshToken(User user)
        {
            RefreshToken refreshToken = GenerateRefreshToken();
            user.RefreshTokens.Add(refreshToken);
            await _context.SaveChangesAsync();

            return refreshToken;
        }

        public async Task<User> GetUserFromAccessToken(string accessToken)
        {
            try
            {
                var tokenHandler = new JwtSecurityTokenHandler();
                var key = Encoding.ASCII.GetBytes(_jwtsettings.SecretKey);

                var tokenValidationParameters = new TokenValidationParameters
                {
                    ValidateIssuerSigningKey = true,
                    IssuerSigningKey = new SymmetricSecurityKey(key),
                    ValidateIssuer = false,
                    ValidateAudience = false
                };

                SecurityToken securityToken;
                var principle = tokenHandler.ValidateToken(accessToken, tokenValidationParameters, out securityToken);

                JwtSecurityToken jwtSecurityToken = securityToken as JwtSecurityToken;

                if (jwtSecurityToken != null && jwtSecurityToken.Header.Alg.Equals(SecurityAlgorithms.HmacSha256, StringComparison.InvariantCultureIgnoreCase))
                {
                    var userId = principle.FindFirst(ClaimTypes.Name)?.Value;

                    return await _context.User.Where(u => u.Id == Convert.ToInt32(userId)).FirstOrDefaultAsync();
                }
            }
            catch (Exception)
            {
                return null;
            }

            return null;
        }

        public bool ValidateRefreshToken(User user, string reToken)
        {

            RefreshToken refreshToken = _context.RefreshToken.Where(rt => rt.Token == reToken)
                                                .OrderByDescending(rt => rt.ExpireDate)
                                                .FirstOrDefault();

            if (refreshToken != null && refreshToken.UserId == user.Id
                && refreshToken.ExpireDate > DateTime.UtcNow)
            {
                return true;
            }

            return false;
        }

        public AccessToken GenerateAccessToken(User user)
        {
            var tokenHandler = new JwtSecurityTokenHandler();
            var key = Encoding.ASCII.GetBytes(_jwtsettings.SecretKey);
            var expire = DateTime.UtcNow.AddDays(1);
            var tokenDescriptor = new SecurityTokenDescriptor
            {
                Subject = new ClaimsIdentity(CreateClaims(user)),
                Expires = expire,
                SigningCredentials = new SigningCredentials(new SymmetricSecurityKey(key),
                SecurityAlgorithms.HmacSha256Signature)
            };
            var token = tokenHandler.CreateToken(tokenDescriptor);

            AccessToken accessToken = new AccessToken();
            accessToken.Token = tokenHandler.WriteToken(token);
            accessToken.ExpireDate = expire;

            return accessToken;
        }

        public CurrentUser CreateCurrentUser(ClaimsPrincipal principal)
        {
            if (principal != null && principal.Identity.IsAuthenticated)
            {
                return CreateCurrentUser((ClaimsIdentity)principal.Identity);
            }

            return null;
        }

        private RefreshToken GenerateRefreshToken()
        {
            RefreshToken refreshToken = new RefreshToken();

            var randomNumber = new byte[32];
            using (var rng = RandomNumberGenerator.Create())
            {
                rng.GetBytes(randomNumber);
                refreshToken.Token = Convert.ToBase64String(randomNumber);
            }
            refreshToken.ExpireDate = DateTime.UtcNow.AddMonths(6);

            return refreshToken;
        }

        private Claim[] CreateClaims(User user)
        {
            return new Claim[]
                {
                    new Claim(ClaimTypes.Name, user.Id.ToString()),
                    new Claim("username", user.Username),
                    new Claim(ClaimTypes.Role, Enum.GetName(typeof(UserRole), user.Role))
                };
        }

        private CurrentUser CreateCurrentUser(ClaimsIdentity identity)
        {
            return new CurrentUser(
                Int32.Parse(identity.FindFirst(ClaimTypes.Name).Value),
                identity.FindFirst("username").Value,
                (UserRole)Enum.Parse(typeof(UserRole), identity.FindFirst(ClaimTypes.Role).Value));
        }

        private string Hash(string input)
        {
            using (SHA1Managed sha1 = new SHA1Managed())
            {
                var hash = sha1.ComputeHash(Encoding.UTF8.GetBytes(input));
                var sb = new StringBuilder(hash.Length * 2);

                foreach (byte b in hash)
                {
                    sb.Append(b.ToString("X2"));
                }

                return sb.ToString();
            }
        }

        private string GenerateRandomPassword()
        {
            Random random = new Random();

            const string chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
            return new string(Enumerable.Repeat(chars, 12)
              .Select(s => s[random.Next(s.Length)]).ToArray());
        }
    }

}
