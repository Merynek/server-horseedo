using Horseedo.Cache;
using Horseedo.Middlewares;
using Horseedo.Models;
using Horseedo.Models.AppSettings;
using Horseedo.Services;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Controllers;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.DependencyInjection.Extensions;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Microsoft.IdentityModel.Tokens;
using Microsoft.OpenApi.Models;
using NAutowired;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Horseedo
{
    public class Startup
    {
        IWebHostEnvironment _env;
        public Startup(IConfiguration configuration, IWebHostEnvironment env)
        {
            Configuration = configuration;
            _env = env;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            var currentEnv = _env.IsDevelopment() ? "Development" : "Production";
            services.AddControllers().AddControllersAsServices();
            services.Replace(ServiceDescriptor.Transient<IControllerActivator, NAutowiredControllerActivator>());

            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", new OpenApiInfo { Title = "My API Merin", Version = "v1" });
            });

            services.AddMvc(option => option.EnableEndpointRouting = false)
                .SetCompatibilityVersion(CompatibilityVersion.Version_3_0)
                .AddNewtonsoftJson(opt => opt.SerializerSettings.ReferenceLoopHandling = Newtonsoft.Json.ReferenceLoopHandling.Ignore);

            services.AddDbContext<HorseedoDBContext>(options =>
                options.UseSqlServer(Configuration.GetConnectionString(currentEnv + ":HorseDB")));

            services.AddScoped<UserService>();
            services.AddScoped<HorseService>();
            services.AddScoped<EmailService>();
            services.AddSingleton<UserCache>();
            services.AddHttpContextAccessor();

            services.Configure<EmailSettings>(Configuration.GetSection("EmailSettings:" + currentEnv));
            services.Configure<ClientSettings>(Configuration.GetSection("ClientSettings:" + currentEnv));
            services.Configure<CompanyInfo>(Configuration.GetSection("CompanyInfo"));

            SetUpAuthentication(services);
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app,
            IHostApplicationLifetime lifetime,
            HorseedoDBContext dbContext,
            ILoggerFactory loggerFactory)
        {
            if (_env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();

                app.UseSwagger();
                app.UseSwaggerUI(c =>
                {
                    c.SwaggerEndpoint("/swagger/v1/swagger.json", "My API Merin");
                });
            }
            else
            {
                dbContext.Database.Migrate();
            }

            app.UseErrorHandlingMiddleware();

            SetupLogging(loggerFactory);

            lifetime.ApplicationStarted.Register(() => OnApplicationStarted(app));

            app.UseHttpsRedirection();

            app.UseRouting();

            app.UseAuthentication();

            app.UseAuthorization();

            app.UseRequestFilter();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
        }

        private void OnApplicationStarted(IApplicationBuilder app)
        {
            using (var serviceScope = app.ApplicationServices.GetService<IServiceScopeFactory>().CreateScope())
            {
                var dbContext = serviceScope.ServiceProvider.GetRequiredService<HorseedoDBContext>();
                var userChache = serviceScope.ServiceProvider.GetRequiredService<UserCache>();
                var startedService = new OnStartedService(dbContext, userChache);
            }
        }

        private void SetUpAuthentication(IServiceCollection services)
        {
            var jwtSection = Configuration.GetSection("JWTSettings");
            services.Configure<JWTSettings>(jwtSection);

            var appSettings = jwtSection.Get<JWTSettings>();
            var key = Encoding.ASCII.GetBytes(appSettings.SecretKey);

            services.AddAuthentication(x =>
            {
                x.DefaultAuthenticateScheme = JwtBearerDefaults.AuthenticationScheme;
                x.DefaultChallengeScheme = JwtBearerDefaults.AuthenticationScheme;
            })
                .AddJwtBearer(x =>
                {
                    x.RequireHttpsMetadata = true;
                    x.SaveToken = true;
                    x.TokenValidationParameters = new TokenValidationParameters
                    {
                        ValidateIssuerSigningKey = true,
                        IssuerSigningKey = new SymmetricSecurityKey(key),
                        ValidateIssuer = false,
                        ValidateAudience = false,
                        ClockSkew = TimeSpan.Zero
                    };
                });
        }

        private void SetupLogging(ILoggerFactory loggerFactory)
        {
            loggerFactory.AddFile("Logs/INFO-{Date}.txt", LogLevel.Information);
            loggerFactory.AddFile("Logs/ERROR-{Date}.txt", LogLevel.Error);
            loggerFactory.AddFile("Logs/CRITIC-{Date}.txt", LogLevel.Critical);
        }
    }

}
