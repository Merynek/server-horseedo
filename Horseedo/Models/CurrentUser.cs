﻿using Horseedo.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Horseedo.Models
{
    public class CurrentUser
    {
        public int Id { get; set; }
        public string Username { get; set; }
        public UserRole Role { get; set; }

        public CurrentUser(int id, string username, UserRole role)
        {
            Id = id;
            Username = username;
            Role = role;
        }

    }
}
