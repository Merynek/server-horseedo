﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Horseedo.Models.Database
{
    public class NotActivatedUser
    {
        public int Id { get; set; }
        public int UserId { get; set; }
        public string Code { get; set; }
        public virtual User User { get; set; }

    }
}
