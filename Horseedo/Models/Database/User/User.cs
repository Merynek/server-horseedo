﻿using Horseedo.Enums;
using Horseedo.Models.Request.Users;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Horseedo.Models.Database
{
    public class User
    {
        public int Id { get; set; }
        public string Username { get; set; }
        public string Name { get; set; }
        public string Surname { get; set; }
        public string Email { get; set; }
        public string PhoneNumber { get; set; }
        public string Password { get; set; }
        public UserRole Role { get; set; }
        public bool Banned { get; set; } = false;
        public bool Activated { get; set; } = false;
        public virtual ICollection<RefreshToken> RefreshTokens { get; set; }

        public User(RegistrationUserRequest req)
        {
            Username = req.Username;
            Email = req.Email;
            PhoneNumber = req.PhoneNumber;
            Name = req.Name;
            Surname = req.Surname;
            Password = req.Password;
            Role = UserRole.USER;
        }

        public User()
        { }

    }
}
