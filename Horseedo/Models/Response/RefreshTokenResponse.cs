﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Horseedo.Models.Response
{
    public class RefreshTokenResponse
    {
        public AccessToken Token { get; set; }
        public string RefreshToken { get; set; }

    }
}
