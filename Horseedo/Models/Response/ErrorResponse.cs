﻿using Horseedo.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Horseedo.Models.Response
{
    public class ErrorResponse
    {
        public ErrorCode ErrorCode { get; set; }
        public string Message { get; set; }
    }

}
