﻿using Horseedo.Models.Database;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Horseedo.Models.Response
{
    public class LoginResponse
    {
        public User User { get; set; }
        public AccessToken Token { get; set; }
        public string RefreshToken { get; set; }

    }
}
