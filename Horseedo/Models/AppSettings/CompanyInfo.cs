﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Horseedo.Models.AppSettings
{
    public class CompanyInfo
    {
        public string Name { get; set; }
        public string Owner { get; set; }
        public string Mobile { get; set; }
        public string MobileNice { get; set; }
        public string Email { get; set; }
        public string AddressStreet { get; set; }
        public string AddressCity { get; set; }
        public string AddressPSC { get; set; }
        public string ICO { get; set; }
        public string BankName { get; set; }
        public string BankNumber { get; set; }

    }
}
