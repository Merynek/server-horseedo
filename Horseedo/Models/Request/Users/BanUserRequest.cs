﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Horseedo.Models.Request
{
    public class BanUserRequest
    {
        [Required]
        [Display(Name = "id_user")]
        public int IdUser { get; set; }

        [Required]
        [Display(Name = "ban")]
        public bool Ban { get; set; }

    }
}
