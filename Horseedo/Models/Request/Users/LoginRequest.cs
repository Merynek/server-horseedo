﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Horseedo.Models.Request
{
    public class LoginRequest
    {
        [Required]
        [Display(Name = "username")]
        public string Username { get; set; }

        [Required]
        [Display(Name = "password")]
        public string Password { get; set; }

    }
}
