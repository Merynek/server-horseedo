﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Horseedo.Models.Request
{
    public class ActiveUserRequest
    {
        [Required]
        [Display(Name = "activateCode")]
        public string ActivateCode { get; set; }

    }
}
