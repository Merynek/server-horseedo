﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Horseedo.Models.Request
{
    public class ChangeUserInfoRequest
    {
        [Required]
        [Display(Name = "phoneNumber")]
        public string PhoneNumber { get; set; }

        [Required]
        [Display(Name = "name")]
        public string Name { get; set; }

        [Required]
        [Display(Name = "surname")]
        public string Surname { get; set; }

    }
}
