﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Horseedo.Models.Request
{
    public class RefreshTokenRequest
    {
        [Required]
        [Display(Name = "token")]
        public string Token { get; set; }

        [Required]
        [Display(Name = "refreshToken")]
        public string RefreshToken { get; set; }

    }
}
