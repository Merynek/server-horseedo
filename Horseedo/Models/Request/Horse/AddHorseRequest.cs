﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Horseedo.Models.Request.Horse
{
    public class AddHorseRequest
    {
        [Required]
        [Display(Name = "name")]
        public string Name { get; set; }
    }
}
