﻿using Horseedo.Models.Database;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Horseedo.Models
{
    public class HorseedoDBContext: DbContext
    {
        public HorseedoDBContext(DbContextOptions<HorseedoDBContext> options) : base(options)
        { }

        public DbSet<Horse> HorseAnimal { get; set; }
        public DbSet<User> User { get; set; }
        public DbSet<RefreshToken> RefreshToken { get; set; }
        public DbSet<NotActivatedUser> NotActivatedUser { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
                optionsBuilder.UseSqlServer("Name=HorseDB");
            }
        }

    }
}
