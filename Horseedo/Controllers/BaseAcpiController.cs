﻿using Horseedo.Models;
using Horseedo.Services;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using NAutowired.Core.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Horseedo.Controllers
{
    [ApiController]
    public class BaseApiController : ControllerBase
    {
        [Autowired]
        protected readonly UserService _userService;
        [Autowired]
        protected readonly ILogger<UsersController> _logger;
        [Autowired]
        protected readonly IHttpContextAccessor _haccess;

        protected CurrentUser _currentUser
        {
            get
            {
                return _userService.CreateCurrentUser(_haccess.HttpContext.User);
            }
        }
    }

}
