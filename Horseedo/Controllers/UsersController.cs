﻿using Horseedo.Enums;
using Horseedo.Exceptions;
using Horseedo.Models.Database;
using Horseedo.Models.Request;
using Horseedo.Models.Response;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Horseedo.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class UsersController : BaseApiController
    {
        [HttpPost("login")]
        public async Task<ActionResult<LoginResponse>> Login(LoginRequest req)
        {
            User user = await _userService.GetUser(req.Username, req.Password);
            if (user == null)
            {
                throw new BadRequestHorseedoException(ErrorCode.LOGIN_FAILED, "Login failed");
            }
            if (user.Banned)
            {
                throw new BadRequestHorseedoException(ErrorCode.BANNED, "User is banned");
            }
            if (!user.Activated)
            {
                throw new BadRequestHorseedoException(ErrorCode.USER_NOT_ACTIVATED, "User is not Activated");
            }

            RefreshToken refreshToken = await _userService.GenerateRefreshToken(user);

            var response = new LoginResponse();
            response.User = user;
            response.RefreshToken = refreshToken.Token;
            response.Token = _userService.GenerateAccessToken(user);

            return response;
        }

        [Authorize]
        [HttpPost("refreshToken")]
        public async Task<ActionResult<RefreshTokenResponse>> RefreshToken(RefreshTokenRequest req)
        {
            User user = await _userService.GetUserFromAccessToken(req.Token);

            if (user != null && _userService.ValidateRefreshToken(user, req.RefreshToken))
            {
                RefreshToken refreshToken = await _userService.GenerateRefreshToken(user);

                var response = new RefreshTokenResponse();
                response.Token = _userService.GenerateAccessToken(user);
                response.RefreshToken = refreshToken.Token;

                return response;
            }

            return null;
        }

        [HttpPost("activate")]
        public async Task<ActionResult> ActiveUser(ActiveUserRequest req)
        {
            _logger.LogInformation("Active user");
            await _userService.ActivateUser(req);
            return Ok();
        }

        [Authorize]
        [HttpPost("ban")]
        public async Task<ActionResult> BanUser(BanUserRequest req)
        {
            _logger.LogInformation("Ban user - ID: {id}", req.IdUser);
            await _userService.BanUser(req.IdUser, req.Ban);
            return Ok();
        }

        [HttpPost("forgetPassword")]
        public async Task<ActionResult> ForgetPassword(ForgetPasswordRequest req)
        {
            _logger.LogInformation("Forget Password on email: {email}", req.Email);
            await _userService.GenerateNewPassword(req);
            return Ok();
        }

        [Authorize]
        [HttpPost("changePassword")]
        public async Task<ActionResult> ChangePassword(ChangePasswordRequest req)
        {
            _logger.LogInformation("Change Password on user: {username}", _currentUser.Username);
            await _userService.ChangePassword(req, _currentUser.Id);
            return Ok();
        }

        [Authorize]
        [HttpPost("changeInfo")]
        public async Task<ActionResult> ChangeInfo(ChangeUserInfoRequest req)
        {
            _logger.LogInformation("Change Info on user: {username}", _currentUser.Username);
            await _userService.ChangeInfo(req, _currentUser.Id);
            return Ok();
        }
    }

}
