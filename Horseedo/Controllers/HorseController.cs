﻿using Horseedo.Models;
using Horseedo.Models.Database;
using Horseedo.Models.Request.Horse;
using Horseedo.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using NAutowired.Core.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Horseedo.Controllers
{
    [Authorize]
    [Route("api/[controller]")]
    [ApiController]
    public class HorseController : BaseApiController
    {
        [Autowired]
        private readonly HorseedoDBContext _context;
        [Autowired]
        private readonly HorseService _horseService;

        // GET: api/Horse
        [HttpGet]
        public async Task<ActionResult<IEnumerable<Horse>>> GetHorse()
        {
            return await _context.HorseAnimal.ToListAsync();
        }

        // GET: api/Horse/5
        [HttpGet("{id}")]
        public async Task<ActionResult<Horse>> GetHorseById(int id)
        {
            var horse = await _context.HorseAnimal
                .Where(h => h.Id == id)
                .FirstOrDefaultAsync();

            if (horse == null)
            {
                return NotFound();
            }

            return horse;
        }

        // PUT: api/Horse/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPut("{id}")]
        public async Task<IActionResult> PutHorse(int horse_id, Horse horse)
        {
            if (horse_id != horse.Id)
            {
                return BadRequest();
            }

            _context.Entry(horse).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!HorseExists(horse_id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/Horse
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPost]
        public async Task<ActionResult<Horse>> NewHorse(AddHorseRequest req)
        {
            var newHorse = await _horseService.AddHorse(req);

            return newHorse;
        }

        // DELETE: api/Horse/5
        [HttpDelete("{id}")]
        public async Task<ActionResult<Horse>> DeleteHorse(int id)
        {
            var horse = await _context.HorseAnimal.FindAsync(id);
            if (horse == null)
            {
                return NotFound();
            }

            _context.HorseAnimal.Remove(horse);
            await _context.SaveChangesAsync();

            return horse;
        }

        private bool HorseExists(int id)
        {
            return _context.HorseAnimal.Any(e => e.Id == id);
        }
    }

}
