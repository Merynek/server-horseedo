﻿using Horseedo.Models.Request.Users;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Horseedo.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class RegistrationController : BaseApiController
    {
        [HttpPost("user")]
        public async Task<ActionResult> RegistrationUser(RegistrationUserRequest req)
        {
            await _userService.RegistrationUser(req);

            _logger.LogInformation("New user - Name: {username}", req.Username);

            return Ok();
        }
    }

}
