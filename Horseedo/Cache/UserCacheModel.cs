﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Horseedo.Cache
{
    public class UserCacheModel
    {
        public int Id { get; set; }
        public bool Banned { get; set; }

        public UserCacheModel(int id, bool banned)
        {
            this.Id = id;
            this.Banned = banned;
        }

    }
}
