﻿using Horseedo.Models.Database;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Horseedo.Cache
{
    public class UserCache
    {
        private ConcurrentDictionary<int, UserCacheModel> _cache = new ConcurrentDictionary<int, UserCacheModel>();

        public void CreateUserCache(List<User> users)
        {
            foreach (User user in users)
            {
                _cache.TryAdd(user.Id, new UserCacheModel(user.Id, user.Banned));
            }
        }

        public void SetBan(int id, bool ban)
        {
            UserCacheModel user = GetUserFromCache(id);
            if (user != null)
            {
                user.Banned = ban;
            }
            else
            {
                _cache.TryAdd(id, new UserCacheModel(id, ban));
            }
            RefreshCache();
        }

        public bool isBanned(int id)
        {
            return GetUserFromCache(id) != null;
        }

        private UserCacheModel GetUserFromCache(int id)
        {
            if (_cache.ContainsKey(id))
            {
                return _cache[id];
            }
            return null;
        }

        private void RefreshCache()
        {
            foreach (var item in _cache.Where(user => !user.Value.Banned))
            {
                _cache.Remove(item.Key, out var x);
            }
        }
    }

}
