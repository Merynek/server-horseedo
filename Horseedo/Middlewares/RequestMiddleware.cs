﻿using Horseedo.Cache;
using Horseedo.Enums;
using Horseedo.Exceptions;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Horseedo.Middlewares
{
    public class RequestMiddleware
    {
        private readonly RequestDelegate _next;

        public RequestMiddleware(RequestDelegate next)
        {
            _next = next;
        }

        public async Task Invoke(HttpContext httpContext, UserCache userCache)
        {
            var user = httpContext.User;
            if (user != null && user.Identity.IsAuthenticated)
            {
                var userId = user.Identity.Name;
                if (userCache.isBanned(Int32.Parse(userId)))
                {
                    throw new UnauthorizedHorseedoException(ErrorCode.BANNED, "User is banned");
                }
                await _next(httpContext);
            }
            else
            {
                await _next(httpContext);
            }
        }
    }

    public static class RequestMiddlewareExtensions
    {
        public static IApplicationBuilder UseRequestFilter(
            this IApplicationBuilder builder)
        {
            return builder.UseMiddleware<RequestMiddleware>();
        }
    }

}
